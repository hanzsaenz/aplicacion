<?php
/* @var $this PersonasController */
/* @var $model Personas */

$this->breadcrumbs=array(
	'Personas'=>array('index'),
	$model->docPersona,
);

$this->menu=array(
	array('label'=>'Lista de Personas', 'url'=>array('index')),
	array('label'=>'Crear Personas', 'url'=>array('create')),
	array('label'=>'Actualizar Personas', 'url'=>array('update', 'id'=>$model->docPersona)),
	array('label'=>'Eliminar Personas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->docPersona),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Personas', 'url'=>array('admin')),
);
?>

<h1>Ver Personas #<?php echo $model->docPersona; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'docPersona',
		'nomPersona',
		'apePersona',
		'areaPersona',
		'dirPersona',
		'emailPersona',
		'telPersona',
	),
)); ?>
