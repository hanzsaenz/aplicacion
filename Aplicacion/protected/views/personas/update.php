<?php
/* @var $this PersonasController */
/* @var $model Personas */

$this->breadcrumbs=array(
	'Personas'=>array('index'),
	$model->docPersona=>array('view','id'=>$model->docPersona),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista de Personas', 'url'=>array('index')),
	array('label'=>'Crear Personas', 'url'=>array('create')),
	array('label'=>'Ver Personas', 'url'=>array('view', 'id'=>$model->docPersona)),
	array('label'=>'Administrar Personas', 'url'=>array('admin')),
);
?>

<h1>Actualizar Personas <?php echo $model->docPersona; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>