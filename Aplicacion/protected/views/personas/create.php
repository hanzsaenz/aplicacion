<?php
/* @var $this PersonasController */
/* @var $model Personas */

$this->breadcrumbs=array(
	'Personases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Lista de Personas', 'url'=>array('index')),
	array('label'=>'Administrar Personas', 'url'=>array('admin')),
);
?>

<h1>Crear Personas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>