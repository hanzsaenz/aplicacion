<?php
/* @var $this PersonasController */
/* @var $data Personas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('docPersona')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->docPersona), array('view', 'id'=>$data->docPersona)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomPersona')); ?>:</b>
	<?php echo CHtml::encode($data->nomPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apePersona')); ?>:</b>
	<?php echo CHtml::encode($data->apePersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('areaPersona')); ?>:</b>
	<?php echo CHtml::encode($data->areaPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dirPersona')); ?>:</b>
	<?php echo CHtml::encode($data->dirPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailPersona')); ?>:</b>
	<?php echo CHtml::encode($data->emailPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telPersona')); ?>:</b>
	<?php echo CHtml::encode($data->telPersona); ?>
	<br />


</div>