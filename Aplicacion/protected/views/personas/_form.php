<?php
/* @var $this PersonasController */
/* @var $model Personas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'personas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Documento'); ?>
		<?php echo $form->textField($model,'docPersona',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'docPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model,'nomPersona',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'nomPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Apellido'); ?>
		<?php echo $form->textField($model,'apePersona',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'apePersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Area'); ?>
		<?php echo $form->textField($model,'areaPersona',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'areaPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Direccion'); ?>
		<?php echo $form->textField($model,'dirPersona',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'dirPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'emailPersona',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'emailPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Telefono'); ?>
		<?php echo $form->textField($model,'telPersona',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'telPersona'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->