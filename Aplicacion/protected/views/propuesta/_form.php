<?php
/* @var $this PropuestaController */
/* @var $model Propuesta */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'propuesta-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<table width="100%" border="1" cellpadding="0" cellspacing="8" bordercolor="#000000">
<tr> 
	<th>&nbsp; </th>
<th> &nbsp;</th>
	<th> 
	<p class="note">Los campos marcados con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Persona'); ?>
		<?php echo $form->dropDownList($model,'docPersona',
			CHtml::listData(Personas::model()->findAll(),'docPersona','nomPersona','apePersona')); ?>
		<?php echo $form->error($model,'docPersona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Colaboradores'); ?>
		<?php echo $form->dropDownList($model,'colaboProyecto',
			CHtml::listData(Personas::model()->findAll(),'docPersona','nomPersona','apePersona')); ?>
		<?php echo $form->error($model,'colaboProyecto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Nombre del Proyecto'); ?>
		<?php echo $form->textField($model,'nomProyecto',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nomProyecto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Descripcion'); ?>
		<?php echo $form->textArea($model,'desProyecto',array('size'=>60,'maxlength'=>500, 'rows'=>10, 'cols'=>61)); ?>
		<?php echo $form->error($model,'desProyecto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Enviar' : 'Save'); ?>
	</div>



</th>

</tr>
<tr> 
<td rowspan="2" valign="middle" align="left">Este texto está alineado al centro 
verticalmente y a la izquierda horizontalmente</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr> 
<td colspan="2">&nbsp;</td>
</tr>
</table>


<?php $this->endWidget(); ?>

</div><!-- form -->