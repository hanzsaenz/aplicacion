<?php
/* @var $this PropuestaController */
/* @var $data Propuesta */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPropuesta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idPropuesta), array('view', 'id'=>$data->idPropuesta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('docPersona')); ?>:</b>
	<?php echo CHtml::encode($data->docPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('colaboProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->colaboProyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->nomProyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desProyecto')); ?>:</b>
	<?php echo CHtml::encode($data->desProyecto); ?>
	<br />


</div>