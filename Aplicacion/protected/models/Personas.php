<?php

/**
 * This is the model class for table "Personas".
 *
 * The followings are the available columns in table 'Personas':
 * @property string $docPersona
 * @property string $nomPersona
 * @property string $apePersona
 * @property string $areaPersona
 * @property string $dirPersona
 * @property string $emailPersona
 * @property string $telPersona
 */
class Personas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Personas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('docPersona, nomPersona, apePersona, areaPersona, dirPersona, emailPersona, telPersona', 'required'),
			array('docPersona, telPersona', 'length', 'max'=>20),
			array('nomPersona, apePersona, areaPersona', 'length', 'max'=>90),
			array('dirPersona, emailPersona', 'length', 'max'=>180),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('docPersona, nomPersona, apePersona, areaPersona, dirPersona, emailPersona, telPersona', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'docPersona' => 'Doc Persona',
			'nomPersona' => 'Nom Persona',
			'apePersona' => 'Ape Persona',
			'areaPersona' => 'Area Persona',
			'dirPersona' => 'Dir Persona',
			'emailPersona' => 'Email Persona',
			'telPersona' => 'Tel Persona',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('docPersona',$this->docPersona,true);
		$criteria->compare('nomPersona',$this->nomPersona,true);
		$criteria->compare('apePersona',$this->apePersona,true);
		$criteria->compare('areaPersona',$this->areaPersona,true);
		$criteria->compare('dirPersona',$this->dirPersona,true);
		$criteria->compare('emailPersona',$this->emailPersona,true);
		$criteria->compare('telPersona',$this->telPersona,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Personas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
