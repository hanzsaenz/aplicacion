<?php

/**
 * This is the model class for table "propuesta".
 *
 * The followings are the available columns in table 'propuesta':
 * @property string $idPropuesta
 * @property string $docPersona
 * @property string $colaboProyecto
 * @property string $nomProyecto
 * @property string $desProyecto
 */
class Propuesta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'propuesta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('docPersona, colaboProyecto, nomProyecto, desProyecto', 'required'),
			array('docPersona', 'length', 'max'=>20),
			array('colaboProyecto', 'length', 'max'=>800),
			array('nomProyecto', 'length', 'max'=>200),
			array('desProyecto', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idPropuesta, docPersona, colaboProyecto, nomProyecto, desProyecto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPropuesta' => 'Id Propuesta',
			'docPersona' => 'Doc Persona',
			'colaboProyecto' => 'Colabo Proyecto',
			'nomProyecto' => 'Nom Proyecto',
			'desProyecto' => 'Des Proyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPropuesta',$this->idPropuesta,true);
		$criteria->compare('docPersona',$this->docPersona,true);
		$criteria->compare('colaboProyecto',$this->colaboProyecto,true);
		$criteria->compare('nomProyecto',$this->nomProyecto,true);
		$criteria->compare('desProyecto',$this->desProyecto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Propuesta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
